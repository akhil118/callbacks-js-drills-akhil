let callback = (boardID, getlists) => {
    setTimeout(() => {
        const lists = require("./data/lists.json")
        const result = getlists(boardID, lists)
        console.log(result)
    }, 2 * 1000);
}


module.exports = callback