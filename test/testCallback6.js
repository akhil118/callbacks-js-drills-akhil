const callback = require('../callback5')
const lists = require('../data/lists.json')
// console.log(lists)
// function getBoardInfo(boardID, boards) {
//     return boards.filter(({id}) => id === boardID);
// }

// function getlists(boardID, lists) {
//     return lists[boardID] === undefined ? [] : lists[boardID];
// }

// function getCards(listID, cards) {
//     return cards[listID] === undefined ? [] : cards[listID];
// }
// console.log( Object.values(lists))
const listIDs = Object.values(lists).reduce((ids, currentList) => {
    ids.push(...currentList.map(({id}) => id))
    // console.log(ids)
    return ids
}, []);

callback('mcu453ed', listIDs);
