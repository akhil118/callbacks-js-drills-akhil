let callback = (boardID, getBoardInfo) => {
    setTimeout(() => {
        const boards = require("./data/boards.json")
        const result = getBoardInfo(boardID, boards)
        console.log(result)
    }, 2 * 1000);
}

module.exports = callback