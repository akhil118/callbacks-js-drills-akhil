

let callback = (boardID, listID) => {
    const callback1 = require('./callback1')
    const callback2 = require('./callback2')
    const callback3 = require('./callback3')
    const getBoardInfo = require("./test/testCallback1")
    const getLists = require("./test/testCallback2")
    const getCards = require("./test/testCallback3")

    setTimeout(() => {callback1(boardID, getBoardInfo)}, 0);
    setTimeout(() => {callback2(boardID, getLists)}, 0);
    setTimeout(() => {callback3(listID, getCards)}, 0);
}

module.exports = callback